package br.com.itau;

import java.util.Random;

public class Main {
    public static void main(String[] args){

        Random randomNumber = new Random();
        System.out.println("teste");
        //Sortear um numero entre 1 e 6.
        System.out.println(randomNumber.nextInt(6) + 1);

        //Sortear 3 números entre 1 e 6 e exibir no console os valores e a soma
        int valor1 = randomNumber.nextInt(6) + 1;
        int valor2 = randomNumber.nextInt(6) + 1;
        int valor3 = randomNumber.nextInt(6) + 1;
        int soma = valor1+valor2+valor3;

        System.out.println("Primeiro valor: " + valor1);
        System.out.println("Segundo valor: " + valor2);
        System.out.println("Terceiro valor: " + valor3);
        System.out.println("Soma " + soma);

        //Sortear tres grupos de 3 números, sempre exibindo o resultado da soma da cada grupo
        for(int i = 0; i<3; i++){
            int valores[]  = new int[3];
            int total = 0;
            System.out.println("Linha "+ i);
            for(int j = 0; j<3; j++){

                valores[j] = randomNumber.nextInt(6) + 1;
                System.out.print(valores[j] + ",");
                total = total+valores[j];
            }
            System.out.println(total);

        }

    }
}
